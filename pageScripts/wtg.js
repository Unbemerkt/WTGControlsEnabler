var wtgControls;
var enableControllsBTN = document.createElement("button");
enableControllsBTN.innerText = "Enable Youtube Controls";
enableControllsBTN.style = "background-color: rgba(255,255,255,0.5); color: #ffffff; cursor: pointer;";

enableControllsBTN.addEventListener("click",()=>{
    var player_iframe = document.getElementsByTagName("iframe")[0];
    setState(0)
    if (player_iframe === null || player_iframe === undefined){
        console.error("[WTG CE] Can't find video player! Please start a video or reload this browser window!")
        setState(2)
    }else{
        if (!player_iframe.src.includes("www.youtube.com")){
            console.error("[WTG CE] Current video isn't a Youtube Video!")
            setState(2)
            return;
        }
        if (player_iframe.src.includes("controls=1")||player_iframe.src.includes("controls=0")){
            if (player_iframe.src.includes("controls=1")){
                setState(0)
                console.info("[WTG CE] Removing controls... Please wait...")
                player_iframe.src = player_iframe.src.replaceAll("controls=1","controls=0")
                console.info("[WTG CE] Controls removed!")
                setTimeout(()=>{
                    setState(3)
                },250)
                return;
            }
            setState(0)
            console.info("[WTG CE] Adding controls... Please wait...")
            player_iframe.src = player_iframe.src.replaceAll("controls=0","controls=1")
            console.info("[WTG CE] Controls added!")
            setTimeout(()=>{
                setState(1)
            },250)
        }else{
            console.error("[WTG CE] Current video isn't a Youtube Video!")
            setState(2)
        }
    }
});

function setState(state){
    const gStyle = "border:none;cursor: pointer;";
    if (state === 0){
        //loading
        enableControllsBTN.style = gStyle+"background-color: rgba(255,255,255,0.5); color: #ffffff;";
        enableControllsBTN.innerText = "Loading...";
    }else if (state === 1){
        //enabled
        enableControllsBTN.style = gStyle+"background-color: rgba(0,255,0,0.5); color: #00FF00;";
        enableControllsBTN.innerText = "Controls Enabled";
    }else if (state === 2){
        //err
        enableControllsBTN.style = gStyle+"background-color: rgba(255,0,0,0.5); color: #FF0000;";
        enableControllsBTN.innerText = "An Error Occurred";
    }else if (state === 3){
        //disabled
        enableControllsBTN.style = gStyle+"background-color: rgba(255,0,0,0.5); color: #FF0000;";
        enableControllsBTN.innerText = "Controls Disabled";
    }
}

function addButton() {
    console.log("[WTG CE] "+window.origin)
    wtgControls = document.getElementById("player_controls");
    if (wtgControls === null || wtgControls === undefined) {
        console.error("[WTG CE] Video container not found!")
        return;
    }

    wtgControls.appendChild(enableControllsBTN);
    setState(3);
    console.info("[WTG CE] Added button!")
}
addButton();
